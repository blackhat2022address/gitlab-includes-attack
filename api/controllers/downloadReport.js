const taxPayer = require("../models/taxPayers");
const libxml = require('libxmljs')
const fs = require('fs')

module.exports = async (req, res, next) => {
    taxPayer.findById(req.query.pan)
        .then(response => {
            if (!response) {
                res.status(400).json({
                    error: "no such user exists"
                })
            } else {
                const x = response.data
                const xmlObject = Buffer.from(x, 'base64').toString()

                const xmlDoc = libxml.parseXml(xmlObject, { noent: true })



                fs.writeFileSync(`${__dirname}\\${req.query.pan}.xml`, xmlDoc.toString())

                res.download(`${__dirname}\\${req.query.pan}.xml`, err => {
                    if (!err) {
                        fs.unlinkSync(`${__dirname}\\${req.query.pan}.xml`)
                    }
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        })
};
