const mongoose = require('mongoose');

const taxPayerSchema = mongoose.Schema({
    "data": String,
    "_id": String
})

module.exports = new mongoose.model("TaxPayer", taxPayerSchema);